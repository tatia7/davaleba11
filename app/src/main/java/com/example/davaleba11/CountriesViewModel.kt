package com.example.davaleba11

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CountriesViewModel : ViewModel() {

    private val countriesLiveData = MutableLiveData<List<Items>>().apply {
        mutableListOf<Items>()
    }
    val _countriesLiveData: LiveData<List<Items>> = countriesLiveData


    private val loadingLiveData = MutableLiveData<Boolean>()
    val _loadingLiveData : LiveData<Boolean> = loadingLiveData
    fun init(){
        CoroutineScope(Dispatchers.IO).launch {
            getCountries()
        }
    }

    private suspend fun getCountries() : MutableList<Items>?{

        val result  = RetrofitService.retrofitService().getCountry()
        if (result.isSuccessful){
            val items = result.body()
            countriesLiveData.postValue(items)
            return items?.toMutableList()
        }else{
            return null
        }
        loadingLiveData.postValue(false)
    }
}