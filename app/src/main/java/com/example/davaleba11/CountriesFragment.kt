package com.example.davaleba11

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.davaleba11.databinding.FragmentCountriesBinding

class CountriesFragment : Fragment() {

    private val countries: CountriesViewModel by viewModels()
    private lateinit var binding: FragmentCountriesBinding
    private lateinit var adapter : recViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCountriesBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }
    private fun init(){
        countries.init()
        initRec()
        observes()

        binding.refresh.setOnRefreshListener {
            adapter.clearData()

        }
    }

    private fun observes(){
        countries._loadingLiveData.observe(viewLifecycleOwner, {
            binding.refresh.isRefreshing = it

        })
        countries._countriesLiveData.observe(viewLifecycleOwner, {
            adapter.setData(it.toMutableList())
        })
    }
    private fun initRec(){
        adapter = recViewAdapter()
        binding.recView.layoutManager = LinearLayoutManager(this@CountriesFragment.context)
        binding.recView.adapter = adapter
    }

}