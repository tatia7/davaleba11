package com.example.davaleba11

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.davaleba11.databinding.ItemsrecBinding
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou

class recViewAdapter() : RecyclerView.Adapter<recViewAdapter.viewHolder>() {

    private val countries = mutableListOf<Items>()
    inner class viewHolder(private val binding: ItemsrecBinding) : RecyclerView.ViewHolder(binding.root){
        private lateinit var model : Items

        fun bind(){
            model = countries[adapterPosition]
            loadSvg(model.flag)
            binding.country.text = model.name
            binding.capital.text = model.capital
            binding.continent.text = model.region
        }
        private fun loadSvg(url: String?) {
            GlideToVectorYou
                .init()
                .with(binding.root.context)
                .load(Uri.parse(url),binding.image)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        return viewHolder(ItemsrecBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = countries!!.size

    fun setData(countrie: MutableList<Items>){
        this.countries.clear()
        this.countries.addAll(countrie)
        notifyDataSetChanged()
    }
    fun clearData(){
        this.countries.clear()
        notifyDataSetChanged()
    }
}