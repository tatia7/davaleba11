package com.example.davaleba11

import android.widget.ImageView

data class Items(val name : String? = null,
                 val capital :String? = null,
                 val region : String ? = null,
                val flag : String ? = null)
